import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private authservice:AuthService,
              private router:Router) { }

  email:string;
  password:string;

  ngOnInit() {
  }

  onSubmit(){
    this.authservice.signUp(this.email, this.password)
  }

}
