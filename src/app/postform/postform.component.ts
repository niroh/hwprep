import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { throwMatDuplicatedDrawerError } from '@angular/material/sidenav';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})
export class PostformComponent implements OnInit {

  constructor(private postsservice:PostsService,
              public activatedroute:ActivatedRoute,
              public router:Router,
              private db:AngularFirestore,
              public authservice:AuthService) { }
    
  title:string;
  author:string;
  id:string;
  isEdit:boolean = false;
  buttonText:string = "Add Post";
  userId:string;

         

  ngOnInit() {
    this.id = this.activatedroute.snapshot.params.id

    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid;
        if(this.id){
          this.isEdit = true;
          this.buttonText = "Update Post";
          this.postsservice.getPost(this.userId, this.id).subscribe(
              post => {
                this.title = post.data().title
                this.author = post.data().author
              })
        }
      }
    )

    
  }

  onSubmit(){
    if(this.isEdit){
      this.postsservice.updatePost(this.userId,this.id, this.title, this.author)
    }
    else{
      this.postsservice.addPost(this.userId, this.title, this.author)
    }
   this.router.navigate(['/posts']);
  }

}
