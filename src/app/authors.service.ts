import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ObserversModule } from '@angular/cdk/observers';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  constructor() { }
  authors:any[] = [{id:1 , name:'Lewis Carrol'},{id:2 ,name:'Leo Tolstoy'}, {id:3, name:'Thomas Mann'}, {id:4 ,name:'Amos Os'}]
  

  getAuthors(){
    const authorsObservable = new Observable(
      observer => {
        setInterval( () => observer.next(this.authors) , 4000)
      }
    )
    return authorsObservable;
  }

  addAuthor(newAuthor:string){
    this.authors.push({name:newAuthor});
  }
}
