import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public afAuth:AngularFireAuth,
              public router:Router) {
    this.user = this.afAuth.authState;
  }

  user: Observable<User | null>

  signUp(email:string, password:string){
    this.afAuth.auth.createUserWithEmailAndPassword(email,password).then(
      user => {this.router.navigate(['/posts'])}
    )
  }

  logout(){
    this.afAuth.auth.signOut().then(user => {this.router.navigate(['/login'])});
  }

  login(email:string, password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password).then(
      user => {this.router.navigate(['/posts'])}
        )
  }
}
