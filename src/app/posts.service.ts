import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Posts } from './interfaces/posts';
import { Users } from './interfaces/users';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http:HttpClient,
              private db:AngularFirestore) { }

  public URLposts = "https://jsonplaceholder.typicode.com/posts/"
  public URLusers = "https://jsonplaceholder.typicode.com/users/"
  
  userCollection: AngularFirestoreCollection = this.db.collection('users');
  postCollection: AngularFirestoreCollection;

  getPosts(userId:string):Observable<any[]>{
   // return this.http.get<Posts[]>(this.URLposts)
   //return this.db.collection('posts').valueChanges({idField:'id'});
   this.postCollection = this.db.collection(`users/${userId}/posts`);
   return this.postCollection.snapshotChanges().pipe(
    map(
      collection => collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )

    )
  )


  }

  getPost(userId:string, id:string):Observable<any>{
    return this.db.doc(`users/${userId}/posts/${id}`).get()  
  }  

  getUsers(){
    //return this.http.get<Users[]>(this.URLusers)
    return this.db.collection('posts').valueChanges();

  }

 /* savePosts(title:string, body:string, author:string){
    const post = {title:title, body:body, author:author}
    this.db.collection('posts').add(post);
  }*/

  addPost(userId:string, title:string,author:string){
    const post = {title:title, author:author}
    //this.db.collection('posts').add(post);
    this.userCollection.doc(userId).collection('posts').add(post);
  }

  updatePost(userId:string, id:string, title:string, author:string){
    this.db.doc(`users/${userId}/posts/${id}`).update(
      {title:title,
      author:author}
    )

  }

  deletePost(userId:string, id:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }
}
