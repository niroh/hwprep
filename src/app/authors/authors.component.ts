import { AuthorsService } from './../authors.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  constructor(private activatedroute:ActivatedRoute,
              private authorsservice:AuthorsService) { }

  //authors:object[] = [{id:1 , name:'Lewis Carrol'},{id:2 ,name:'Leo Tolstoy'}, {id:3, name:'Thomas Mann'}, {id:4 ,name:'Amos Os'}]
  //id:number;
  //authorName:string;
  authors$:Observable<any>;
  authorInput:string

  ngOnInit() {
    //this.id = this.activatedroute.snapshot.params['id'];
    //this.authorName = this.activatedroute.snapshot.params['authorName'];
    this.authors$ = this.authorsservice.getAuthors();
  }

  onSubmit(){
    this.authorsservice.addAuthor(this.authorInput)
  }

  

}
