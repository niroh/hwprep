import { AuthService } from './../auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Posts } from '../interfaces/posts';
import { Users } from '../interfaces/users';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  constructor(public postsservice:PostsService,
              private db:AngularFirestore,
              public authservice:AuthService) { }

  posts$:Observable<any>;
  userId:string
 
  ngOnInit() {
    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid;
        this.posts$ = this.postsservice.getPosts(this.userId);
      }
    )
 
  }

  updatePost(id:string,title:string,author:string){
    this.postsservice.updatePost(id, title, author, this.userId);
  }

  getBook(id:string):Observable<any>{
    return this.db.doc(`posts/${id}`).get()  
  }  

  deletePost(id:string){
    this.postsservice.deletePost(id, this.userId);
  }


}