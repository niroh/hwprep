import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {

  constructor(private router:Router,
              private activatedroute: ActivatedRoute) { }
  id:number;
  authorInput:string;

  ngOnInit() {
    this.id = this.activatedroute.snapshot.params['id']
    this.authorInput = this.activatedroute.snapshot.params['authorName']
  }

  onSubmit(){
    this.router.navigate(['/authors', this.id, this.authorInput])
  }


}
