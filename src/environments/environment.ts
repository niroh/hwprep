// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCmS6l4aE0RiV6eaJrf0uAZZkcZkmeytMI",
    authDomain: "hwprep-25dc8.firebaseapp.com",
    databaseURL: "https://hwprep-25dc8.firebaseio.com",
    projectId: "hwprep-25dc8",
    storageBucket: "hwprep-25dc8.appspot.com",
    messagingSenderId: "856372497019",
    appId: "1:856372497019:web:d487fe51854cb01bf270de"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
